import { browser, by, element, ExpectedConditions as EC } from 'protractor';

export class RegistrationPage {
  navigateTo() {
    return browser.get('/login/new');
  }
  get usernameInput() {
    return element(by.css('#username'));
  }
  get fullNameInput() {
    return element(by.css('#fullName'));
  }
  get passwordInput() {
    return element(by.css('#password'));
  }
  get repeatPasswordInput() {
    return element(by.css('#repeatPassword'));
  }
  get submitButton() {
    return element(by.css(`button[type='submit']`));
  }
  get form() {
    return element(by.name('form'));
  }
  submit() {
    browser.wait(EC.elementToBeClickable(this.submitButton), 5000);
    return this.submitButton.click();
  }
}
