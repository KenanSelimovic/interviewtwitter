import { browser } from 'protractor';

import { RegistrationPage } from './registration.po';

describe('interview-twitter-ui Registration', () => {
  let page: RegistrationPage;

  beforeEach(() => {
    page = new RegistrationPage();
  });

  it('should redirect to tweets', async() => {
    page.navigateTo();
    const randomness = Date.now() + Math.random().toString();
    page.usernameInput.sendKeys(`username-${randomness}`);
    page.fullNameInput.sendKeys(`fullName-${randomness}`);
    page.passwordInput.sendKeys(`password`);
    page.repeatPasswordInput.sendKeys(`password`);

    await page.submit();
    expect(browser.getCurrentUrl()).toContain('/app/tweets');
  });
});
