import { TweetModel } from '../app/models/tweet.model';
import { UserProfileModel } from '../app/models/user-profile.model';

export const testTweetModel = (): TweetModel => ({
  id: 1,
    author: {
    id: 1,
      fullName: 'author name',
      username: 'author',
  },
  content: 'tweetContent',
});

export const testUserProfileModel = (): UserProfileModel => ({
  id: 1,
  fullName: 'author name',
  username: 'author',
  numFollowers: 1,
  numFollowing: 3,
  numTweets: 4,
});
