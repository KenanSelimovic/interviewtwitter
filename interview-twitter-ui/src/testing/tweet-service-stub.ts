import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { TweetModel } from '../app/models/tweet.model';
import { testTweetModel } from './test-data';

export class TweetServiceStub {
  fetch(): Observable<TweetModel[]> {
    return of([ testTweetModel() ]);
  }

  fetchForUser(username: string): Observable<TweetModel[]> {
    return of([ testTweetModel() ]);
  }

  create(tweetContent: string): Observable<TweetModel> {
    return of({ ...testTweetModel(), content: tweetContent });
  }
}
