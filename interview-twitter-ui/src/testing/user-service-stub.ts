import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { testUserProfileModel } from './test-data';
import { UserProfileModel } from '../app/models/user-profile.model';

export class UserServiceStub {
  fetch(): Observable<UserProfileModel> {
    return of(testUserProfileModel());
  }
}
