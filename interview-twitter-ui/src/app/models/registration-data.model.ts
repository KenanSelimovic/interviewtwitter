export interface RegistrationDataModel {
  username: string,
  fullName: string,
  password: string,
}
