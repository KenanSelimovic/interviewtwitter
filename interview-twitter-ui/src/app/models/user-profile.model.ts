import { UserModel } from './user.model';

export interface UserProfileModel extends  UserModel {
  numFollowers: number,
  numFollowing: number,
  numTweets: number,
}
