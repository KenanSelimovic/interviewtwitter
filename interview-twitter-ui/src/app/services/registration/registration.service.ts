import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import { AuthService } from '../auth.service';
import { RegistrationDataModel } from '../../models/registration-data.model';
import { UserModel } from '../../models/user.model';

@Injectable()
export class RegistrationService {

  constructor(private http: HttpClient, private authService: AuthService) { }

  register(username: string, fullName: string, password: string) {
    const registrationData: RegistrationDataModel = { username, fullName, password };
    return this.http.post<UserModel>('/api/register', registrationData)
      .map(() => {
        this.authService.login(username, password);
      });
  }

}
