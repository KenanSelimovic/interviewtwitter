import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { UserProfileModel } from '../../models/user-profile.model';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  fetch(): Observable<UserProfileModel> {
    return this.http.get<UserProfileModel>('/api/me');
  }

}
