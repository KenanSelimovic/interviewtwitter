import { inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { TweetService } from './tweet.service';

describe('TweetService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ TweetService ],
      imports: [ HttpClientTestingModule ],
    });
  });

  it('should be created', inject([TweetService], (service: TweetService) => {
    expect(service).toBeTruthy();
  }));
});
