import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { UserProfileModel } from '../../../models/user-profile.model';
import { UserService } from '../../../services/user/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  $user: Observable<UserProfileModel>;

  profileItems: { field: string, text: string }[] = [
    { field: 'username', text: 'Username' },
    { field: 'fullName', text: 'Full name' },
    { field: 'numFollowers', text: 'Number of followers' },
    { field: 'numFollowing', text: 'Number of people following' },
    { field: 'numTweets', text: 'Number of tweets' },
  ];

  constructor(private userService: UserService) {
  }

  ngOnInit() {
    this.$user = this.userService.fetch();
  }

}
