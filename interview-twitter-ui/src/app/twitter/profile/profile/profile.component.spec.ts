import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs/observable/of';

import { ProfileComponent } from './profile.component';
import { ProfileItemComponent } from '../profile-item/profile-item.component';
import { UserService } from '../../../services/user/user.service';
import { UserServiceStub } from '../../../../testing/user-service-stub';
import { testUserProfileModel } from '../../../../testing/test-data';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileComponent, ProfileItemComponent ],
      providers: [ { provide: UserService, useClass: UserServiceStub } ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('displays all user profile values', () => {
    const userProfile = testUserProfileModel();

    component.$user = of(userProfile);
    fixture.detectChanges();

    const divElement: HTMLElement = fixture.nativeElement;
    const usernameItem = divElement.querySelector('.username').textContent;
    const fullNameItem = divElement.querySelector('.fullName').textContent;
    const numFollowersItem = divElement.querySelector('.numFollowers').textContent;
    const numFollowingItem = divElement.querySelector('.numFollowing').textContent;
    const numTweetsItem = divElement.querySelector('.numTweets').textContent;

    expect(usernameItem).toContain(userProfile.username);
    expect(fullNameItem).toContain(userProfile.fullName);
    expect(numFollowersItem).toContain(userProfile.numFollowers.toString(10));
    expect(numFollowingItem).toContain(userProfile.numFollowing.toString(10));
    expect(numTweetsItem).toContain(userProfile.numTweets.toString());
  });
});
