import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { ProfileComponent } from './profile/profile.component';
import { ProfileContainerComponent } from './profile-container/profile-container.component';
import { ProfileItemComponent } from './profile-item/profile-item.component';
import { UserService } from '../../services/user/user.service';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '', component: ProfileContainerComponent, children: [
          {path: '', component: ProfileComponent},
        ],
      },
    ]),
    CommonModule
  ],
  declarations: [ProfileComponent, ProfileContainerComponent, ProfileItemComponent],
  providers: [ UserService ]
})
export class ProfileModule { }
