import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileItemComponent } from './profile-item.component';

describe('ProfileItemComponent', () => {
  let component: ProfileItemComponent;
  let fixture: ComponentFixture<ProfileItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display passed text and value', () => {
    const testText = 'test text';
    const testValue = 'test value';

    component.text = testText;
    component.value = testValue;
    fixture.detectChanges();

    const divElement: HTMLElement = fixture.nativeElement;
    const textItem = divElement.querySelector('.text').textContent;
    const valueItem = divElement.querySelector('.value').textContent;
    expect(textItem).toContain(testText);
    expect(valueItem).toContain(testValue);
  });
});
