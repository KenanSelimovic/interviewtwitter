import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Component, Input } from '@angular/core';

import { UserTweetsViewComponent } from './user-tweets-view.component';
import { TweetService } from '../../../services/tweet/tweet.service';
import { TweetServiceStub } from '../../../../testing/tweet-service-stub';
import { TweetModel } from '../../../models/tweet.model';

describe('UserTweetsViewComponent', () => {
  let component: UserTweetsViewComponent;
  let fixture: ComponentFixture<UserTweetsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTweetsViewComponent, CreateTweetStubComponent, UserTweetsTableStubComponent ],
      providers: [ {provide: TweetService, useClass: TweetServiceStub }],
      imports: [ RouterTestingModule ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTweetsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
@Component({selector: 'app-create-tweet', template: ''})
class CreateTweetStubComponent {}
@Component({selector: 'app-user-tweets-table', template: ''})
class UserTweetsTableStubComponent {
  @Input() tweets: TweetModel[];
}
