import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTweetsTableComponent } from './user-tweets-table.component';
import { testTweetModel } from '../../../../testing/test-data';

describe('UserTweetsTableComponent', () => {
  let component: UserTweetsTableComponent;
  let fixture: ComponentFixture<UserTweetsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserTweetsTableComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTweetsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('displays user full name', () => {
    const tweet = testTweetModel();
    component.tweets = [ tweet ];
    fixture.detectChanges();

    const tableElement: HTMLElement = fixture.nativeElement;
    const tableItems = tableElement.querySelector('td.fullName').textContent;
    expect(tableItems).toContain(tweet.author.fullName);
  });
});
