import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { TweetsTableComponent } from './tweets-table.component';
import { testTweetModel } from '../../../../testing/test-data';

describe('TweetsTableComponent', () => {
  let component: TweetsTableComponent;
  let fixture: ComponentFixture<TweetsTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TweetsTableComponent ],
      imports: [ RouterTestingModule ],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetsTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('displays user full name', () => {
    const tweet = testTweetModel();
    component.tweets = [ tweet ];
    fixture.detectChanges();

    const tableElement: HTMLElement = fixture.nativeElement;
    const tableItems = tableElement.querySelector('td.fullName').textContent;
    expect(tableItems).toContain(tweet.author.fullName);
  });
});
