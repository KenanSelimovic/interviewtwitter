import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input } from '@angular/core';

import { TweetsViewComponent } from './tweets-view.component';
import { TweetService } from '../../../services/tweet/tweet.service';
import { TweetServiceStub } from '../../../../testing/tweet-service-stub';
import { TweetModel } from '../../../models/tweet.model';

describe('TweetsViewComponent', () => {
  let component: TweetsViewComponent;
  let fixture: ComponentFixture<TweetsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TweetsViewComponent, CreateTweetStubComponent, TweetsTableStubComponent ],
      providers: [ { provide: TweetService, useClass: TweetServiceStub } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TweetsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
@Component({selector: 'app-create-tweet', template: ''})
class CreateTweetStubComponent {}
@Component({selector: 'app-tweets-table', template: ''})
class TweetsTableStubComponent {
  @Input() tweets: TweetModel[];
}
