import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { RegistrationDataModel } from '../../models/registration-data.model';
import { RegistrationService } from '../../services/registration/registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent {
  model: RegistrationDataModel & {repeatPassword: string} = { username: '', fullName: '', password: '', repeatPassword: '' };
  loading = false;

  constructor(private router: Router,
              private registrationService: RegistrationService) {
  }

  onSubmit(registrationForm: NgForm): void {
    if (registrationForm.valid) {
      this.register();
    }
  }

  register() {
    this.loading = true;
    const { username, fullName, password } = this.model;
    this.registrationService.register(username, fullName, password)
      .subscribe(() => {
        this.router.navigate(['/app/tweets']);
      });
  }

  isFormSubmittedWithInvalidUsername(registrationForm: NgForm): boolean {
    const usernameFormControl = registrationForm.form.controls['username'];
    return registrationForm.submitted && usernameFormControl && !usernameFormControl.valid;
  }

  isFormSubmittedWithInvalidFullName(registrationForm: NgForm): boolean {
    const fullNameFormControl = registrationForm.form.controls['fullName'];
    return registrationForm.submitted && fullNameFormControl && !fullNameFormControl.valid;
  }

  isFormSubmittedWithInvalidPassword(registrationForm: NgForm): boolean {
    const passwordFormControl = registrationForm.form.controls['password'];
    return registrationForm.submitted && passwordFormControl && !passwordFormControl.valid;
  }
}
