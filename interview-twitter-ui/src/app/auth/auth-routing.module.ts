import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginContainerComponent } from './login-container/login-container.component';
import { RegistrationContainerComponent } from './registration-container/registration-container.component';

const routes: Routes = [
  {
    path: 'new',
    component: RegistrationContainerComponent,
  },
  {
    path: '',
    component: LoginContainerComponent,
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {
}
