package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.controller.dto.ErrorMessage;
import com.javalanguagezone.interviewtwitter.service.UserService;
import com.javalanguagezone.interviewtwitter.service.dto.UserDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserProfileDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserRegistrationDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collection;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;

@RestController
@Slf4j
public class UserController {

  private UserService userService;

  public UserController(UserService userService) {
    this.userService = userService;
  }

  @GetMapping("/followers")
  public Collection<UserDTO> followers(Principal principal) {
    return userService.getUsersFollowers(principal);
  }

  @GetMapping("/following")
  public Collection<UserDTO> following(Principal principal) {
    return userService.getUsersFollowing(principal);
  }

  @GetMapping("/me")
  public UserProfileDTO me(Principal principal) {
    return userService.getUserProfile(principal);
  }

  @PostMapping("/register")
  @ResponseStatus(CREATED)
  public UserDTO register(@RequestBody UserRegistrationDTO userData) {
    return userService.registerUser(userData);
  }

  @ExceptionHandler
  @ResponseStatus(BAD_REQUEST)
  public ErrorMessage handleInvalidUserException(UserService.InvalidUserException e){
    log.warn("", e);
    return new ErrorMessage("Invalid user data");
  }
  @ExceptionHandler
  @ResponseStatus(BAD_REQUEST)
  public ErrorMessage handlePasswordTooShortException(UserService.PasswordTooShortException e){
    log.warn("", e);
    return new ErrorMessage("Provided password is too short");
  }
  @ExceptionHandler
  @ResponseStatus(BAD_REQUEST)
  public ErrorMessage handleUsernameTakenException(UserService.UsernameTakenException e){
    log.warn("", e);
    return new ErrorMessage("'username " + e.getUsername() + " already taken'");
  }
}
