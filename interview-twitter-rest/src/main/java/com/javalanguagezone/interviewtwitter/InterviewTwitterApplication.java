package com.javalanguagezone.interviewtwitter;

import com.javalanguagezone.interviewtwitter.domain.Tweet;
import com.javalanguagezone.interviewtwitter.domain.User;
import com.javalanguagezone.interviewtwitter.repository.TweetRepository;
import com.javalanguagezone.interviewtwitter.repository.UserRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class InterviewTwitterApplication {

  public static void main(String[] args) {
    SpringApplication.run(InterviewTwitterApplication.class, args);
  }

  @Component
  public static class BootstrapBitcoinEvangelists implements ApplicationRunner {

    private UserRepository userRepository;
    private TweetRepository tweetRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public BootstrapBitcoinEvangelists(UserRepository userRepository, TweetRepository tweetRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
      this.userRepository = userRepository;
      this.tweetRepository = tweetRepository;
      this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) {
      String encodedPassword = bCryptPasswordEncoder.encode("password");
      User rogerVer = userRepository.save(new User("rogerkver", encodedPassword, "Roger Ver"));
      User andreasAntonopoulos = userRepository.save(new User("aantonop", encodedPassword, "Andreas Antonopoulos"));
      User vitalikButerin = userRepository.save(new User("VitalikButerin", encodedPassword, "Vitalik Buterin"));
      User charlieLee = userRepository.save(new User("SatoshiLite", encodedPassword, "Charlie Lee"));
      User satoshiNakamoto = userRepository.save(new User("satoshiNakamoto", encodedPassword, "Satoshi Nakamoto"));

      vitalikButerin.addFollowing(satoshiNakamoto, rogerVer);
      userRepository.save(vitalikButerin);

      rogerVer.addFollowing(satoshiNakamoto, andreasAntonopoulos);
      userRepository.save(rogerVer);

      andreasAntonopoulos.addFollowing(satoshiNakamoto, rogerVer, vitalikButerin, charlieLee);
      userRepository.save(andreasAntonopoulos);

      charlieLee.addFollowing(satoshiNakamoto, rogerVer, vitalikButerin);
      userRepository.save(charlieLee);

      tweetRepository.save(new Tweet("I created Bitcoin!", satoshiNakamoto));
      tweetRepository.save(new Tweet("I'm an alias", satoshiNakamoto));
      tweetRepository.save(new Tweet("Bitcoin cash is true Bitcoin!", rogerVer));
      tweetRepository.save(new Tweet("ETH is all about smart contracts", vitalikButerin));
      tweetRepository.save(new Tweet("Most of the ICO's will fail", andreasAntonopoulos));
      tweetRepository.save(new Tweet("Cryptocurrencies are all about disruption", andreasAntonopoulos));
      tweetRepository.save(new Tweet("Bitcoin mining is based on Proof of Work", andreasAntonopoulos));
      tweetRepository.save(new Tweet("If Bitcoin is  crypto gold then Litecoin is crypto silver.", charlieLee));
    }
  }
  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
