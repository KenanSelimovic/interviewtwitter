package com.javalanguagezone.interviewtwitter.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PUBLIC;

@Getter
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor(access = PUBLIC)
public class UserRegistrationDTO {
  private String username;
  private String fullName;
  private String password;
}
