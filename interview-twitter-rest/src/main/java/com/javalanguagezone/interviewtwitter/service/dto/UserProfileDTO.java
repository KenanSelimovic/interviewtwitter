package com.javalanguagezone.interviewtwitter.service.dto;

import com.javalanguagezone.interviewtwitter.domain.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import static lombok.AccessLevel.PRIVATE;

@Getter
@NoArgsConstructor(access = PRIVATE)
public class UserProfileDTO extends UserDTO {
  private int numFollowers;
  private int numFollowing;
  private int numTweets;

  public UserProfileDTO(User user) {
    super(user);
    this.numFollowers = user.getFollowers().size();
    this.numFollowing = user.getFollowing().size();
    this.numTweets = user.getTweets().size();
  }
}
