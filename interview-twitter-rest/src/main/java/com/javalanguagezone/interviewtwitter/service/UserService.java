package com.javalanguagezone.interviewtwitter.service;

import com.javalanguagezone.interviewtwitter.domain.User;
import com.javalanguagezone.interviewtwitter.repository.UserRepository;
import com.javalanguagezone.interviewtwitter.service.dto.UserDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserProfileDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserRegistrationDTO;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

@Service
public class UserService implements UserDetailsService {

  private UserRepository userRepository;
  private final BCryptPasswordEncoder bCryptPasswordEncoder;

  public UserService(UserRepository userRepository, final BCryptPasswordEncoder bCryptPasswordEncoder) {
    this.userRepository = userRepository;
    this.bCryptPasswordEncoder = bCryptPasswordEncoder;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = getUser(username);
    if(user == null)
      throw new UsernameNotFoundException(username);
    return user;
  }

  @Transactional
  public Collection<UserDTO> getUsersFollowing(Principal principal) {
    User user = getUser(principal.getName());
    return convertUsersToDTOs(user.getFollowing());
  }

  @Transactional
  public Collection<UserDTO> getUsersFollowers(Principal principal) {
    User user = getUser(principal.getName());
    return convertUsersToDTOs(user.getFollowers());
  }

  @Transactional
  public UserProfileDTO getUserProfile(Principal principal) {
    User user = getUser(principal.getName());
    return convertUserToProfileDTO(user);
  }

  @Transactional
  public UserDTO registerUser(UserRegistrationDTO registrationDTO) {
    if (registrationDTO.getPassword().length() < User.PASSWORD_MIN_LENGTH) {
      throw new PasswordTooShortException();
    }

    User existingUser = userRepository.findOneByUsername(registrationDTO.getUsername());
    if (existingUser != null) {
      throw new UsernameTakenException(registrationDTO.getUsername());
    }

    String hashedPassword = bCryptPasswordEncoder.encode(registrationDTO.getPassword());
    User user = new User(registrationDTO.getUsername(), hashedPassword, registrationDTO.getFullName());
    if (!user.isValid()) {
      throw new InvalidUserException();
    }
    User inserted = userRepository.save(user);
    return new UserDTO(inserted);
  }

  private User getUser(String username) {
    return userRepository.findOneByUsername(username);
  }

  private List<UserDTO> convertUsersToDTOs(Set<User> users) {
    return users.stream().map(UserDTO::new).collect(toList());
  }

  private UserProfileDTO convertUserToProfileDTO(User user) {
    return new UserProfileDTO(user);
  }

  public static class InvalidUserException extends RuntimeException {
    private InvalidUserException() {
      super("'invalid registration data provided'");
    }
  }

  public static class PasswordTooShortException extends RuntimeException {
    private PasswordTooShortException() {
      super("password too short");
    }
  }

  public static class UsernameTakenException extends RuntimeException {
    private String username;
    private UsernameTakenException(String username) {
      super(username);
      this.username = username;
    }
    public String getUsername() {
      return username;
    }
  }

}
