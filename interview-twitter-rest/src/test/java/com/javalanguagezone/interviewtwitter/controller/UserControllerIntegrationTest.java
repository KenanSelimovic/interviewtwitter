package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.domain.User;
import com.javalanguagezone.interviewtwitter.repository.UserRepository;
import com.javalanguagezone.interviewtwitter.service.dto.TweetDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserProfileDTO;
import com.javalanguagezone.interviewtwitter.service.dto.UserRegistrationDTO;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.beans.HasProperty.hasProperty;
import static org.hamcrest.core.Every.everyItem;
import static org.junit.Assert.assertThat;

public class UserControllerIntegrationTest extends RestIntegrationTest {
  @Autowired
  private UserRepository userRepository;

  @Test
  public void followersRequested_allFollowersReturned() {
    ResponseEntity<UserDTO[]> response = withAuthTestRestTemplate().getForEntity("/followers", UserDTO[].class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    List<UserDTO> followers = Arrays.asList(response.getBody());
    assertThat(followers, hasSize(1));
    assertThat(extractUsernames(followers), contains("rogerkver"));
  }

  @Test
  public void followersRequested_fullNamePropertyReturned() {
    ResponseEntity<UserDTO[]> response = withAuthTestRestTemplate().getForEntity("/followers", UserDTO[].class);
    List<UserDTO> followers = Arrays.asList(response.getBody());
    assertThat(followers, everyItem(hasProperty("fullName")));
  }

  @Test
  public void getFollowingFromFirstPage() {
    ResponseEntity<UserDTO[]> response = withAuthTestRestTemplate().getForEntity("/following", UserDTO[].class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
    List<UserDTO> following = Arrays.asList(response.getBody());
    assertThat(following, hasSize(4));
    assertThat(extractUsernames(following), containsInAnyOrder(followingUsersUsernames()));
  }

  @Test
  public void followingRequested_fullNamePropertyReturned() {
    ResponseEntity<UserDTO[]> response = withAuthTestRestTemplate().getForEntity("/following", UserDTO[].class);
    List<UserDTO> following = Arrays.asList(response.getBody());
    assertThat(extractFullNames(following), containsInAnyOrder(followingUsersFullNames()));
  }

  @Test
  public void userProfileRequested_returnsCurrentUserInfo() {
    ResponseEntity<UserProfileDTO> response = withAuthTestRestTemplate().getForEntity("/me", UserProfileDTO.class);
    UserProfileDTO user = response.getBody();

    assertThat(user.getUsername(), equalTo(getUsernameOfAuthUser()));
    assertThat(user.getNumFollowing(), equalTo(followingUsers().length));
    assertThat(user.getNumFollowers(), equalTo(getNumFollowers()));
    assertThat(user.getNumTweets(), equalTo(getNumTweets()));
  }

  @Test
  public void userRegistrationWithShortPassword_requestRejected() {
    UserRegistrationDTO registrationDTO = new UserRegistrationDTO(
      "username_short_password",
      "full name_short_password",
      "a"
    );
    ResponseEntity<UserDTO> registrationResponse = testRestTemplate.postForEntity("/register", registrationDTO, UserDTO.class);
    assertThat(registrationResponse.getStatusCode().is4xxClientError(), is(true));
  }

  @Test
  public void userRegistrationWithEmptyUsername_requestRejected() {
    UserRegistrationDTO registrationDTO = new UserRegistrationDTO(
      "",
      "full name_empty_username",
      "password"
    );
    ResponseEntity<UserDTO> registrationResponse = testRestTemplate.postForEntity("/register", registrationDTO, UserDTO.class);
    assertThat(registrationResponse.getStatusCode().is4xxClientError(), is(true));
  }

  @Test
  public void userRegistrationAlreadyTakenUsername_requestRejected() {
    UserRegistrationDTO registrationDTO = generateRegistrationData();
    UserRegistrationDTO invalidRegistrationData = new UserRegistrationDTO(
      registrationDTO.getUsername(),
      "email taken fullName",
      "email taken password"
    );
    testRestTemplate.postForEntity("/register", registrationDTO, UserDTO.class);
    ResponseEntity<UserDTO> registrationResponse = testRestTemplate.postForEntity("/register", invalidRegistrationData, UserDTO.class);
    assertThat(registrationResponse.getStatusCode().is4xxClientError(), is(true));
  }

  @Test
  public void userRegistration_userStoredInDB() {
    UserRegistrationDTO registrationDTO = generateRegistrationData();
    ResponseEntity<UserDTO> response = testRestTemplate.postForEntity("/register", registrationDTO, UserDTO.class);
    UserDTO user = response.getBody();

    assertThat(user.getUsername(), equalTo(registrationDTO.getUsername()));
    User inserted = userRepository.findOneByUsername(registrationDTO.getUsername());
    assertThat(inserted, notNullValue());
    assertThat(inserted.getUsername(), equalTo(registrationDTO.getUsername()));
  }

  @Test
  public void userRegistration_possibleToAccessProtectedEndpointAfterwards() {
    UserRegistrationDTO registrationDTO = generateRegistrationData();
    ResponseEntity<UserDTO> registrationResponse = testRestTemplate.postForEntity("/register", registrationDTO, UserDTO.class);
    assertThat(registrationResponse.getStatusCode().is2xxSuccessful(), is(true));

    final ResponseEntity<TweetDTO[]> response = testRestTemplate.withBasicAuth(registrationDTO.getUsername(), registrationDTO.getPassword()).getForEntity("/tweets", TweetDTO[].class);
    assertThat(response.getStatusCode().is2xxSuccessful(), is(true));
  }

  private List<String> extractUsernames(List<UserDTO> users) {
    return users.stream().map(UserDTO::getUsername).collect(toList());
  }
  private List<String> extractFullNames(List<UserDTO> users) {
    return users.stream().map(UserDTO::getFullName).collect(toList());
  }
}
