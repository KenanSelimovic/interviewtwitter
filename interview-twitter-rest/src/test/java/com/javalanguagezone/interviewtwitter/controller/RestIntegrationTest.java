package com.javalanguagezone.interviewtwitter.controller;

import com.javalanguagezone.interviewtwitter.domain.User;
import com.javalanguagezone.interviewtwitter.service.dto.UserRegistrationDTO;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public abstract class RestIntegrationTest {

  private final User[] followingUsers = {
    new User("rogerkver", "password", "Roger Ver"),
    new User("satoshiNakamoto", "password", "Satoshi Nakamoto"),
    new User("SatoshiLite", "password", "Charlie Lee"),
    new User("VitalikButerin", "password", "Vitalik Buterin"),
  };
  @Autowired
  protected TestRestTemplate testRestTemplate;

  TestRestTemplate withAuthTestRestTemplate() {
    return testRestTemplate.withBasicAuth("aantonop", "password");
  }

  String getUsernameOfAuthUser(){
    return "aantonop";
  }

  int getNumFollowers(){
    // value calculated from test seed data
    return 1;
  }

  int getNumTweets(){
    // value calculated from test seed data
    return 3;
  }

  User[] followingUsers(){
    return followingUsers;
  }
  String[] followingUsersUsernames(){
    return Arrays.stream(followingUsers).map(User::getUsername).toArray(String[]::new);
  }
  String[] followingUsersFullNames(){
    return Arrays.stream(followingUsers).map(User::getFullName).toArray(String[]::new);
  }

  UserRegistrationDTO generateRegistrationData() {
    final String randomness = String.valueOf(new Date().getTime()) + Math.random();
    return new UserRegistrationDTO(
      "username" + randomness,
      "full name" + randomness,
      "password"
    );
  }
}
